import hudson.model.*
import hudson.plugins.git.*
import hudson.plugins.git.extensions.GitSCMExtension
import org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import jenkins.model.Jenkins

def env = System.getenv()
String keynesJobName=env.KEYNES_SEED_GITLAB_JOB_NAME
String keynesBranchSpec=env.KEYNES_SEED_GITLAB_BRANCH_SPEC
String keynesGitUrl=env.KEYNES_SEED_GITLAB_GITURL
String credentialsId = ''

if (!Jenkins.instance.getJobNames().contains(keynesJobName)) {
  List<BranchSpec> branchSpec = Collections.singletonList(new BranchSpec(keynesBranchSpec));
  List<SubmoduleConfig> submoduleConfig = Collections.<SubmoduleConfig>emptyList();
  List<UserRemoteConfig> userRemoteConfig = Collections.singletonList(new UserRemoteConfig(keynesGitUrl, '', '', credentialsId))
  List<GitSCMExtension> gitScmExt = new ArrayList<GitSCMExtension>()
  def scm = new GitSCM(userRemoteConfig, branchSpec, false, submoduleConfig, null, null, gitScmExt)

  WorkflowJob job = new WorkflowJob(Jenkins.instance, keynesJobName);
  job.setDescription('Bootstraps the Jenkins server by installing all jobs (using the jobDSL plugin)')
  job.setDefinition(new CpsScmFlowDefinition(scm, 'Jenkinsfile'))
  job.save()

  Jenkins.instance.restart()
}

