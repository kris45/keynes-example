import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration
import jenkins.model.Jenkins

def plugin = Jenkins.instance.getDescriptorByType(GlobalJobDslSecurityConfiguration)
plugin.useScriptSecurity = false
plugin.save()
