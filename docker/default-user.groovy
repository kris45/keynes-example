def env = System.getenv()

String keynesUser=env.KEYNES_USER
String keynesPass=env.KEYNES_PASS

if (!hudson.model.User.get(keynesUser, false)) {
  user = hudson.model.User.get(keynesUser)
  password = hudson.security.HudsonPrivateSecurityRealm.Details.fromPlainPassword(keynesPass)
  user.addProperty(password)
  user.save()
}
