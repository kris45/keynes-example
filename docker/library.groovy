import java.util.Collections
import jenkins.plugins.git.GitSCMSource
import org.jenkinsci.plugins.workflow.libs.GlobalLibraries
import org.jenkinsci.plugins.workflow.libs.LibraryConfiguration
import org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever

def env = System.getenv()

libraryName='keynes'
libraryGitUrl=env.KEYNES_GITURL

gl = GlobalLibraries.get()

if (gl.getLibraries().isEmpty()) {
  gl.setLibraries(Collections.singletonList(
  new LibraryConfiguration(libraryName, new SCMSourceRetriever(new GitSCMSource(libraryGitUrl)))))
}
