# Keynes example

Purpose of this repository is to show how to use
[Keynes](https://gitlab.com/sbitio/keynes) library.

This repository contains:

 * Example of all Keynes features
 * Instructions to set up Jenkins with JobDSL and the Keynes library
 * Dockerfile and groovy scripts to build a Jenkins image ready to run
 a Keynes-based project

## Manual setup of Jenkins to run a Keynes-based project

### 0. Install jenkins

...

### 1. Install dependencies

Install the plugins listed in the Keynes README.

### 2. Declare global library

Go to `Jenkins > Manage Jenkins > Configure System > Global Pipeline Libraries`
and fill at least:

```
Name: keynes
Project Repository: https://gitlab.com/sbitio/keynes
```

### 3. Create the seed job

Go to `Jenkins > New item`. Create a job of type `Pipeline` and fill at least:

```
Definition: Pipeline script from SCM
SCM: Git
Repository URL: ...
```

### 4. Allow JobDSL to run

Default security policy forbids execution of JobDSL code. You want
to install and configure [Authorize Project](https://plugins.jenkins.io/authorize-project)
plugin.

Alternatively you can disable JobDSL security, at your own risk.

### 5. Run it

Run the seed job.

## Docker

`docker/` subfolder contains a Dockerfile based on [jenkins/jenkins:lts](https://hub.docker.com/r/jenkins/jenkins)
that sets up Jenkins to run a Keynes-based project by implementing
the above instructions. For convenience it also creates user `keynes`
with pass `keynes`.

Note on security: for simplicity we opted to disable JobDSL security,
so we don't recomend to use this image in a production environment
(patches welcome).

The Dockerfile installs two seed jobs:

 * `seed-gitlab` linked to https://gitlab.com/sbitio/keynes-example
 * `seed-local` linked to the local directory. Useful for a development workflow

You can copy `docker/` folder to your own project and quickly set up an
instance to test and troubleshot your pipelines.

### Instructions

### Build the image

```bash
cd docker
sudo docker build -t keynes .
```

### Run the container

The bind mount is only neccesary to work with `seed-local`.

```bash
cd docker
sudo docker run --name keynes \
-p 8080:8080 \
--volume keynes:/var/jenkins_home \
--mount type=bind,source="$(pwd)"/..,target=/seed \
keynes
```

### Cleanup

```bash
sudo docker container rm keynes
sudo docker image rm keynes
sudo docker volume rm keynes
```

### Rebuild

```bash
sudo docker container rm keynes && sudo docker image rm keynes && sudo docker volume rm keynes && sudo docker build -t keynes .
```

## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/keynes).

Please log issues for any bug report, feature or support request.

## License

MIT License, see LICENSE file

## Contact

Please use the contact form on http://sbit.io

